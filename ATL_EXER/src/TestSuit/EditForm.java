package TestSuit;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EditForm {
	WebDriver driver ;
	public EditForm(WebDriver driver) {
		this.driver=driver;	
		
		}
	public IssueDetail editForm(){
		
		driver.findElement(By.id("summary")).sendKeys("Editing the issue");
		driver.findElement(By.id("description")).sendKeys("Dummy JIRA Issue");
		driver.findElement(By.id("edit-issue-submit")).click();
		
		Boolean element=(new WebDriverWait(driver, 10))
		  .until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-issue-dialog")));
		
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		IssueDetail issueDetailPage= new IssueDetail(driver);
		return issueDetailPage;
		
		
		
	}
}

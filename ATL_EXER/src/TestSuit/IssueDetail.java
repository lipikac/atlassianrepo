package TestSuit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IssueDetail {
	WebDriver driver ;
	public IssueDetail(WebDriver driver) {
		this.driver=driver;	
		
		}
	public EditForm editIssue(){
		driver.findElement(By.id("edit-issue")).click();
		(new WebDriverWait(driver, 10))
		  .until(ExpectedConditions.presenceOfElementLocated(By.id("summary")));
		Assert.assertTrue(driver.getPageSource().contains("summary"));
		
		EditForm editForm=new EditForm(driver);
		return editForm;
	}
	
	public void setHome(){
		
		
		WebElement issueLink1 = (new WebDriverWait(driver, 20))
	       .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='logo']/a")));
		driver.findElement(By.xpath("//*[@id='logo']/a")).click();
		WebElement issueLink = (new WebDriverWait(driver, 10))
		  .until(ExpectedConditions.presenceOfElementLocated(By.id("quicklinks-content")));
		
	}
	
	
}

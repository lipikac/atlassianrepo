package TestSuit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login {
	WebDriver driver ;
	public Login(WebDriver driver) {
		this.driver=driver;	
		Assert.assertTrue(driver.getPageSource().contains("Log In"));
		
	}
	
	//Function to click on "log in" and go to user password page 
	public UserPass FirstPage(){
		driver.findElement(By.id("user-options")).click();
		WebElement myDynamicElement = (new WebDriverWait(driver, 10))
		  .until(ExpectedConditions.presenceOfElementLocated(By.id("login-submit")));
		Assert.assertTrue(driver.getPageSource().contains("Sign in with Google"));
		
//		UserPass UserPassObj=new UserPass(driver);
		return new UserPass(driver);
	
	}
}

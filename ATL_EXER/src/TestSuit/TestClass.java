package testSuite;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;




public class TestClass {
	
	static WebDriver driver ;
	
	UserPass userPass = null;
	HomePage homePage=null;
	IssueDetail issueDetailPage= null;
	FillIssueForm fillIssueForm=null;
	EditForm editForm=null;
	
	@Before
	public  void  before()
	{
		final String dir = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", dir + "/src/lib/chromedriver.exe");
		driver= new ChromeDriver();
		driver.get("https://jira.atlassian.com/browse/TST");
		driver.manage().deleteAllCookies();
			
	}
	
	@After
	public  void after()
	{
		driver.quit();
		driver=null;
		
	}
	
	public void loginInJira(){
		
		//link opens to first page with login link in right corner 
		Login login = new Login(driver);
		userPass=login.FirstPage();
		
		//to enter user password
		homePage=userPass.loginWithValidDetails();
		
	}
	
	
	@Test
	//Create issue in JIRA
	public void CreateIssue(){
		loginInJira();
		fillIssueForm=homePage.clickCreate();
		issueDetailPage=fillIssueForm.fillIssueForm();
					
	}
	@Test
	//Search an Issue in JIRA
	public void SearchIssue(){
		loginInJira();
		issueDetailPage=homePage.searchIssue();		
		Assert.assertEquals("TST-65439",(driver.findElement(By.id("key-val")).getText()));
	}
	
	@Test
	//Edit an Issue in JIRA
	public void EditIssue(){
		loginInJira();
		issueDetailPage=homePage.searchIssue();			
		editForm=issueDetailPage.editIssue();	
		editForm.editForm();
		homePage.searchIssue();
		Assert.assertEquals("Editing the issue",(driver.findElement(By.id("summary-val")).getText()));
		
	}
}

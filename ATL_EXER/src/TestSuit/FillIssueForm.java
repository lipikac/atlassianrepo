package TestSuit;


import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class FillIssueForm {
	
	WebDriver driver ;
	public FillIssueForm(WebDriver driver){
		this.driver=driver;	
		
	}
	
	public IssueDetail fillIssueForm(){
		

		driver.findElement(By.id("summary")).sendKeys("Testcase for creating JIRA defect");
		
	    Select securityLevel = new Select(driver.findElement(By.id("security")));
	    securityLevel.selectByValue("10032");
	      
	   
	    driver.findElement(By.id("priority-field")).click();
	    driver.findElement(By.xpath("//*[@id='priority-suggestions']/div/ul/li[4]/a")).click();
	    
	    driver.findElement(By.id("assignee-field")).click();
	    driver.findElement(By.xpath("//*[@id='suggestions']/li[1]/a")).click();
	    WebElement issueLink1 = (new WebDriverWait(driver, 20))
       .until(ExpectedConditions.elementToBeClickable(By.id("create-issue-submit")));

	    driver.findElement(By.id("create-issue-submit")).click();
	    
	
    WebElement issueLink = (new WebDriverWait(driver, 20))
	               .until(ExpectedConditions.elementToBeClickable(By.cssSelector(".issue-created-key.issue-link")));
	    
	    issueLink.click();
	    (new WebDriverWait(driver, 50))
               .until(ExpectedConditions.presenceOfElementLocated(By.id("key-val")));
		

	          
	  
		IssueDetail issueDetailPage= new IssueDetail(driver);
		System.out.println("done");
	    return issueDetailPage;
		
	}
	
}

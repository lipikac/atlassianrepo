package TestSuit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {

	WebDriver driver ;
	public HomePage(WebDriver driver) {
		this.driver=driver;	
		//Assert.assertTrue(driver.getPageSource().contains("Create"));
		
	}
	public FillIssueForm clickCreate(){
		
	
		driver.findElement(By.id("create-menu")).click();
		WebElement myDynamicElement = (new WebDriverWait(driver, 10))
		  .until(ExpectedConditions.presenceOfElementLocated(By.id("create-issue-submit")));
		Assert.assertTrue(driver.getPageSource().contains("Summary"));
		
		FillIssueForm fillIssueForm = new FillIssueForm(driver);
		return fillIssueForm;
	}
	
	public IssueDetail searchIssue(){
		driver.findElement(By.id("quickSearchInput")).sendKeys("TST-65439");
		driver.findElement(By.id("quickSearchInput")).sendKeys(Keys.RETURN);
		(new WebDriverWait(driver, 10))
		  .until(ExpectedConditions.presenceOfElementLocated(By.id("summary-val")));
	    
		IssueDetail issueDetailPage= new IssueDetail(driver);
		return issueDetailPage;
	}
	
	
}
